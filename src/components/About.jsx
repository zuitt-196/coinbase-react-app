import React from 'react';
import styled  from 'styled-components';
import {FaArrowAltCircleRight} from  'react-icons/fa';
import about from '../assets/about.png';
import Button from './Button';

const About = () => {
  return (
    <Section className='flex a-center gap '>
      <div className="content container flex column gap-1">
                <div className="subtitle subdue">
                    <h3>About us</h3>
                </div>
                <div className="title">
                    <h2>Why woul You chose Market Watching</h2>
                </div>
                <div className="flex gap">
                    <div className='info flex column gap a-start j-center'>
                        <p className='subdue'>Easy To Learn Platform</p>
                        <p className='subdue'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat obcaecati iusto, pariatur optio laudantium dicta aliquid culpa quasi. Eligendi enim dolorem quis fuga libero debitis nisi laborum aut. Quidem, quas?</p>
                        <Button text="Start Earning" icon={<FaArrowAltCircleRight/>}/>
                    </div>

                    <div className="image">
                        <img src={about} alt="about" className='half-width'/>
                    </div>
                </div>
        </div>
    </Section>
  )
}


const Section = styled.section`
`;
export default About
