import React from 'react';
import styled from 'styled-components';
import card from '../assets/card.png';
import singupBackground from '../assets/signupBackground.png';
const Signup = () => {
  return (
    <Section className='flex gap j-between'>
      <div className='content text-center'>
        <h2>Sign up without any bank account linking and card </h2>
      </div>
      <div className='image'>
        <img src={card} alt="card" />
      </div>
    </Section>
  )
}
const Section = styled.section`
    background-image: url(${singupBackground});
    background-size: contain;
    margin: 0;
    margin-bottom: 8rem;
    max-width:100vw;
    overflow: hidden;
   
   .content{
    padding: 8rem;
   }
   h2{
    font-size: 2.4rem;
   }
   .image{
    height:100%;
   }
`;

export default Signup
