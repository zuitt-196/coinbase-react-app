import React from 'react'
import  styled  from 'styled-components'
import {FaArrowAltCircleRight} from  'react-icons/fa';
import home from '../assets/home.png';
import badgePercent from '../assets/badge-percent.png';
import Button from './Button';

const Home = () => {
  return (
    <Section className='flex j-center a-center gap'> 
        <div className='content flex column gap-2'>
            <div className="subtitle">
                <h3 className="flex a-center gap-1 blue">
                        <span>
                            <img src={badgePercent} alt="percent" />
                        </span>
                    Invesment made easy   
                </h3>
            </div>

            {/* TTITLE SECTION */}
            <div className="title">
                <h1>The Easie Way to Track multilple Currencies</h1>
            </div>

            <div className="description">
                <p className="subdue">
                    Markte Watvman allow to monitor limits and earn rewards for specific coins
                </p>
            </div>

            {/*BUTTON SECTION  */}
                <div className="button flex gap-1">
                    <Button text="try now" icon={<FaArrowAltCircleRight/>} />
                    <Button text="How it work" subduedButton/>
                </div>
        </div>

        {/*IMAGE SECION */}
        <div className="image ">
            <img src={home} alt="home" className='half-width'/>
        </div>
    </Section>
  )
}

export default Home

const Section = styled.section`
  @media screen and (min-width: 280px) and (max-width: 1080px) {
    .subtitle {
      h3 {
        flex-direction: row;
      }
    }
    .buttons {
      flex-direction: row;
      margin: 2rem 0;
      gap: 1rem;
    }
  }
`;

;