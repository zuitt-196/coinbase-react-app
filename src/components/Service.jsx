import React from 'react'
import styled from 'styled-components'
import service1 from '../assets/service1.png';
import  service2 from '../assets/service2.png';
import service3 from '../assets/service3.png';

const Service = () => {
    const servicesData = [
        {
          image: service1,
          title: "Manage your portfolio",
          descrption:
            "Coinbase supports a variety of the most popular digital currencies.",
        },
        {
          image: service2,
          title: "Recurring buys",
          descrption:
            "Coinbase supports a variety of the most popular digital currencies.",
        },
        {
          image: service3,
          title: "Mobile apps",
          descrption:
            "Coinbase supports a variety of the most popular digital currencies.",
        },
      ];
  return (
    <Section className='flex column j-center a-center gap m-top'>
        <div className="title container column gap">
            <div className="title text-center">
                <h2>Start your trading with us</h2>
            </div>
            <div className="subtitle text-center subdue m-botom">
                <h3>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magni nam maiores, quae sed incidunt adipisci libero mag</h3>
            </div>
            <div className="services flex j-center a-center gap m-botom">
                {servicesData.map(({image ,title, descrption}, i) => (
                     <div className="service text-center flex column gap-1" key={i}>
                        <div className="image">
                                <img src={image} alt="service" />
                        </div> 
                        <h3 className='title'>{title}</h3>
                        <p className='description subdue'>
                            {descrption}
                        </p>
                     </div>
                ))}
            </div>
        </div>
    </Section>
  )
}

const Section = styled.section`
    .services{
        padding: 0 5rem;
        gap:10rem;
    .service {
        .title{
            font-size: 1.5rem;
        }
        .description{
            font-size: 1rem;
            line-height: 1.2rem;
        }
    }
    }
`;
export default Service
