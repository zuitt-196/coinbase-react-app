import React from 'react';
import  styled from 'styled-components';
import {AiOutlineMail} from "react-icons/ai"
import Button from './Button';

const NewsLetter = () => {
  return (
    <Section className="flex j-between a-center gap">
            <div className="title-container flex column gap-1">
                        <h2>Newsltter</h2>
                        <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium eos fugit quibusdam,</h3>
            </div>

            <div className='newsletter flex j-centee a-center gap-2'>
                <div className='input-container flex j-center a-center gap-1'>
                    <AiOutlineMail/>
                    <input type="text" placeholder='Enter your email address' />
                </div>
                <Button text="Subcribe"/>

            </div>

    </Section>
  )
}


const Section = styled.section`
    background-color: var(--dark-background);
    margin: 0;
    margin-bottom: 8rem;
    padding:8rem;
    .newsletter{
        .input-container{
            background-color: var(--input-background);
            padding: 1rem;
            padding-right: 8rem;
            border-radius: 0.5rem;
            font-size:1.3rem;
            color:white;
            input{
                border:none;
                background-color: transparent;
                font-size: 1.2rem;
                color:white;
                &:focus{
                    outline:none;
                }
            }
        }
    }



`;
export default NewsLetter