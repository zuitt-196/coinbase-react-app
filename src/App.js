import About from "./components/About";
import DailyPrice from "./components/DailyPrice";
import Footer from "./components/Footer";
import Home from "./components/Home";
import Navbar from "./components/Navbar";
import NewsLetter from "./components/NewsLetter";
import RoadMap from "./components/RoadMap";
import Service from "./components/Service";
import Signup from "./components/Signup";



function App() {
  return (
    <div>
      <Navbar/>
      <Home/>
      <DailyPrice/>
      <About/>
      <Service/>
      <Signup/>
      <RoadMap/>
      <NewsLetter/>
      <Footer/>
    </div>
  );
  
}

export default App;
